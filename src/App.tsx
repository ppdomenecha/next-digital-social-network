import './App.css';

import { BrowserRouter, Route, RouteObject, Routes } from 'react-router-dom';

import PageNotFound from './pages/notFound';
import { appRoutes } from './routes';

function App() {
    const renderNestedRoutes = (route: RouteObject) => {
        return (
            <Route key={route.path} path={route.path} element={route.element}>
                {route.children?.map(renderNestedRoutes)}
            </Route>
        );
    };
    return (
        <BrowserRouter>
            <Routes>
                {appRoutes.map(renderNestedRoutes)}
                <Route path={'/*'} element={<PageNotFound />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
