import { useQuery } from 'react-query';

import { getUser, getUserAlbums, getUsers } from '../services/users.service';

export const useUsersQuery = () => {
    return useQuery('users-query', () => getUsers());
};

export const useUserQuery = (id: number | undefined) => {
    return useQuery(['user-query', id], () => (id ? getUser(id) : undefined));
};

export const useUserAlbumsQuery = (id: number | undefined) => {
    return useQuery(['user-albums-query', id], () =>
        id ? getUserAlbums(id) : undefined
    );
};
