import { useQuery } from 'react-query';

import { getPhotos } from '../services/photos.service';

export const usePhotosQuery = (id: number) => {
    return useQuery(`photos-query-${id}`, () => getPhotos(id));
};
