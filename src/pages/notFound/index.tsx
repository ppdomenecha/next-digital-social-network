import Box from '@mui/material/Box';

export default function PageNotFound() {
    return (
        <Box
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}
        >
            404 PageNotFound
        </Box>
    );
}
