import { CircularProgress, Typography } from '@mui/material';

import { useUserAlbumsQuery } from '../../queries/users-query';
import { UserDetailsAlbumPhotos } from './details-album-photos';

interface IUserDetailsAlbumsProps {
    id: number;
}
export const UserDetailsAlbums = (props: IUserDetailsAlbumsProps) => {
    const { data: albums, isLoading: isLoadingAlbumns } = useUserAlbumsQuery(
        props.id
    );

    if (isLoadingAlbumns) return <CircularProgress />;

    if (!albums)
        return <Typography variant='h5'>No hay álbumes disponibles</Typography>;
    return (
        <>
            {albums.map((a) => (
                <UserDetailsAlbumPhotos key={`album-${a.id}`} album={a} />
            ))}
        </>
    );
};
