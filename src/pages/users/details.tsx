import { Card, CardContent, CircularProgress, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';

import { useUserQuery } from '../../queries/users-query';
import { UserDetailsAlbums } from './details-albums';

export const UserDetails = () => {
    const { id: userId } = useParams();
    const { data: user, isLoading: isLoadingUser } = useUserQuery(
        userId ? parseInt(userId) : undefined
    );

    if (isLoadingUser) return <CircularProgress />;

    if (!user) return <Typography variant='h3'>User not found</Typography>;

    return (
        <Card variant='outlined'>
            <CardContent>
                <Typography variant='h5'>{user.name}</Typography>
                <Typography variant='subtitle1' color='textSecondary'>
                    @{user.username}
                </Typography>
                <Typography variant='body1'>Email: {user.email}</Typography>
                <Typography variant='body1'>Phone: {user.phone}</Typography>
                <Typography variant='body1'>Website: {user.website}</Typography>
                <UserDetailsAlbums id={user.id}></UserDetailsAlbums>
            </CardContent>
        </Card>
    );
};
