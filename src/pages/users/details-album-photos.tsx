import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    CircularProgress,
    Typography
} from '@mui/material';
import React, { useState } from 'react';

import { Album } from '../../models/albums.model';
import { usePhotosQuery } from '../../queries/photos-query';

interface IUserDetailsAlbumPhotosProps {
    album: Album;
}

export const UserDetailsAlbumPhotos = (props: IUserDetailsAlbumPhotosProps) => {
    const [expanded, setExpanded] = useState(false);

    const handleAccordionChange = (
        event: React.SyntheticEvent,
        isExpanded: boolean
    ) => {
        setExpanded(isExpanded);
    };

    return (
        <Accordion expanded={expanded} onChange={handleAccordionChange}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography variant='h6'>{props.album.title}</Typography>
            </AccordionSummary>
            {expanded && (
                <AccordionDetails>
                    <AlbumPhotos album={props.album} />
                </AccordionDetails>
            )}
        </Accordion>
    );
};

const AlbumPhotos = (props: IUserDetailsAlbumPhotosProps) => {
    const { data: photos, isLoading: isLoadingPhotos } = usePhotosQuery(
        props.album.id
    );

    if (isLoadingPhotos) return <CircularProgress />;
    if (!photos)
        return <Typography variant='h5'>No hay fotos disponibles</Typography>;

    return (
        <>
            {photos.map((p) => (
                <Box key={`photo-${p.id}`}>
                    <img src={p.thumbnailUrl} alt={p.title}></img>
                </Box>
            ))}
        </>
    );
};
