import VisibilityIcon from '@mui/icons-material/Visibility';
import { CircularProgress, IconButton } from '@mui/material';
import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useNavigate } from 'react-router-dom';

import { User } from '../../models/user.model';
import { useUsersQuery } from '../../queries/users-query';

const UserListPage = () => {
    const { data: users, isLoading: isLoadingUsers } = useUsersQuery();
    const navigate = useNavigate();

    const columns: GridColDef<User>[] = [
        {
            field: 'id',
            headerName: 'ID',
            width: 20
        },
        {
            field: 'name',
            headerName: 'Name',
            flex: 1
        },
        {
            field: 'username',
            headerName: 'UserName',
            flex: 1
        },
        {
            field: 'actions',
            headerName: '',
            width: 96,
            renderCell: ({ id }) => (
                <>
                    <IconButton
                        onClick={() => {
                            navigate(`${id}`);
                        }}
                    >
                        <VisibilityIcon />
                    </IconButton>
                </>
            )
        }
    ];

    return (
        <Box sx={{ display: 'flex', flexDirection: 'column', gap: '8px' }}>
            <Box sx={{ height: 400, width: '100%' }}>
                {isLoadingUsers ? (
                    <CircularProgress />
                ) : (
                    <DataGrid
                        rows={users ?? []}
                        columns={columns}
                        initialState={{
                            pagination: {
                                paginationModel: {
                                    pageSize: 10
                                }
                            }
                        }}
                        pageSizeOptions={[5, 10, 25]}
                        checkboxSelection
                        disableRowSelectionOnClick
                    />
                )}
            </Box>
        </Box>
    );
};

export default UserListPage;
