import { RouteObject } from 'react-router-dom';

import AppLayout from '../layouts';
import { UserDetails } from '../pages/users/details';
import UserListPage from '../pages/users/index';

export const appRoutes: RouteObject[] = [
    {
        path: '/',
        element: <AppLayout />,
        children: [
            {
                path: '',
                element: <UserListPage />
            },
            {
                path: ':id/',
                element: <UserDetails />
            }
        ]
    }
];
