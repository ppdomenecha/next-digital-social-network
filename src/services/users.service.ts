import axios from 'axios';

import { Album } from '../models/albums.model';
import { User } from '../models/user.model';
const BASE_URL = 'https://jsonplaceholder.typicode.com/users';
export const getUser = async (id: number) => {
    const { data } = await axios.get<User>(`${BASE_URL}/${id}`);

    return data;
};

export const getUsers = async () => {
    const { data } = await axios.get<User[]>(BASE_URL);

    return data;
};

export const getUserAlbums = async (id: number) => {
    const { data } = await axios.get<Album[]>(`${BASE_URL}/${id}/albums`);

    return data;
};
