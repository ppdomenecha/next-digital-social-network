import axios from 'axios';

import { Photo } from '../models/photo.model';
const BASE_URL = 'https://jsonplaceholder.typicode.com/photos';

export const getPhotos = async (albumId: number) => {
    const { data } = await axios.get<Photo[]>(`${BASE_URL}?albumId=${albumId}`);

    return data;
};
