import { Card } from '@mui/material';
import Box from '@mui/material/Box/Box';
import { useState } from 'react';
import { Outlet } from 'react-router-dom';

import AppHeader from './header';
import AppMenu from './menu';

const AppLayout = () => {
    const [menuOpen, setMenuOpen] = useState(true);

    return (
        <>
            <AppHeader toggleMenu={() => setMenuOpen(!menuOpen)} />
            <Box className='container-fluid'>
                <Box
                    sx={{
                        display: 'flex'
                    }}
                >
                    <AppMenu menuOpen={menuOpen} />

                    <main
                        style={{
                            padding: '16px',
                            width: '100%'
                        }}
                    >
                        <Card
                            sx={{
                                padding: '16px'
                            }}
                        >
                            <Outlet />
                        </Card>
                    </main>
                </Box>
            </Box>
        </>
    );
};

export default AppLayout;
