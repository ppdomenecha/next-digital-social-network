import MenuIcon from '@mui/icons-material/Menu';
import { IconButton } from '@mui/material';
import Box from '@mui/material/Box/Box';

interface IAppHeaderProps {
    toggleMenu: () => void;
}

const AppHeader = (props: IAppHeaderProps) => {
    return (
        <>
            <Box
                sx={{
                    height: '48px',
                    background: '#1A97F5',
                    display: 'flex',
                    flexDirection: 'row'
                }}
            >
                <IconButton onClick={props.toggleMenu}>
                    <MenuIcon />
                </IconButton>
                Here the nav bar
            </Box>
        </>
    );
};

export default AppHeader;
