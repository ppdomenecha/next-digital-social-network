import PersonIcon from '@mui/icons-material/Person';
import { IconButton } from '@mui/material';
import Box from '@mui/material/Box/Box';
import { Link } from 'react-router-dom';

interface IAppMenuProps {
    menuOpen: boolean;
}

const AppMenu = (props: IAppMenuProps) => {
    const links = [
        {
            path: '',
            icon: <PersonIcon />,
            displayName: 'Usuarios'
        }
    ];

    return (
        <>
            <Box
                sx={{
                    height: 'calc(100vh - 48px)',
                    width: props.menuOpen ? '250px' : '48px',
                    background: '#FF0000'
                }}
            >
                {links.map((l) => (
                    <Link
                        key={l.path}
                        to={l.path}
                        style={{
                            textDecoration: 'none'
                        }}
                    >
                        <IconButton>
                            {l.icon} {props.menuOpen && l.displayName}
                        </IconButton>
                    </Link>
                ))}
            </Box>
        </>
    );
};

export default AppMenu;
